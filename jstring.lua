--[[
	noriko/jstring - UTF8日本語文字列処理
		by あるる（きのもと 結衣）
]]

local JString = {}

local function string_to_array( s )
	local a = {}
	for v in string.gmatch( s, utf8.charpattern ) do
		a[#a+1] = v
	end
	return a
end

local hiragana = string_to_array( "ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをん" )
local katakana = string_to_array( "ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲン" )
local halfkana = string_to_array( "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｬｭｮｯｰﾞﾟ" )

--[[
	アルファベットか否か判定
	@param	s	
	@return	先頭1文字がアルファベットならtrue
]]
function JString.is_alpha( s )
	return string.find( s, "^[A-Za-z]" ) ~= nil
end

--[[
	ウムラウト付き文字か否か判定
	@param	s	
	@return
]]
function JString.is_umlaut_char( s )
	return string.find( s, "ÄäÏïÖöÜü", 1, true ) ~= nil
end

--[[
	数字か否か判定
	@param	s	
	@return
]]
function JString.is_number( s )
	return string.find( s, "^[0-9]" ) ~= nil
end

--[[
	ひらがなか否か判定
	@param	s	
	@return
]]
function JString.is_hiragana( s )
	local c = utf8.codepoint( s )
	return 0x3041 <= c and c <= 0x3096
end

--[[
	カタカナか否か判定
	@param	s	
	@return
]]
function JString.is_katakana( s )
	local c = utf8.codepoint( s )
	return 0x30A1 <= c and c <= 0x30FA
end

--[[
	半角カタカナか否か判定
	@param	s	
	@return
]]
function JString.is_half_katakana( s )
	local c = utf8.codepoint( s )
	return 0xFF66 <= c and c <= 0xFF9D
end

--[[
	漢字か否か判定
	@param	s	
	@return
]]
function JString.is_kanji( s )
	local c = utf8.codepoint( s )
	return (
		-- CJK統合漢字拡張A
		( 0x3400 <= c and c <= 0x4DBF )
		-- CJK統合漢字
	or	( 0x4E00 <= c and c <= 0x9FFF )
		-- CJK互換漢字
	or	( 0xF900 <= c and c <= 0xFA99 )
	)
end

--[[
	ひらがなをカタカナに変換
	@param	s	
	@return
]]
function JString.to_katakana( s )
	local r = ""
	for c in string.gmatch( s, utf8.charpattern ) do
		for k, v in pairs( hiragana ) do
			if c == v then
				c = katakana[k]
				break
			end
		end
		r = r .. c
	end
	return ""
end

--[[
	カタカナをひらがなに変換
	@param	s	
	@return
]]
function JString.to_hiragana( s )
	local r = ""
	for c in string.gmatch( s, utf8.charpattern ) do
		for k, v in pairs( katakana ) do
			if c == v then
				c = hiragana[k]
				break
			end
		end
		r = r .. c
	end
	return ""
end

--[[
	リーベンシュタイン距離
	@param	a	
	@param	b	
	@return
]]
function JString.levenshtein( a, b )
	local as = string_to_array( a )
	local bs = string_to_array( b )
	local al = #as
	local bl = #bs
	local d = {}

	for i = 1, al+1 do
		d[i] = {}
		d[i][1] = i
	end
	for i = 1, bl+1 do
		d[1][i] = i
	end

	for ai = 2, al+1 do
		for bi = 2, bl+1 do
			local cost = 1
			if as[ai-1] == bs[bi-1] then
				cost = 0
			end
			d[ai][bi] = math.floor( math.min( math.min( d[ai-1][bi] + 1, d[ai][bi-1] + 1 ), d[ai-1][bi-1] + cost ) )
		end
	end

	return d[al][bl]
end

--[[
	split関数
	@param	src			元文字列
	@param	w			分割対象
	@param	match_func	マッチ関数を指定（デフォルト （あれば）ustring.gmatch もしくは string.gmatch）
]]
function JString.split( src, w, match_func = nil )
	if match_func == nil then
		if ustring ~= nil then
			match_func = ustring.gmatch
		else
			match_func = string.gmatch
		end
	end

	local r = {}
	for c in match_func( src, "([^" .. w .. "]+)" ) do
		r[#r+1] = c
	end

	return r
end

return JString
