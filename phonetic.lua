--[[
	noriko/phonetic - Phonetic アルゴリズム
		by あるる（きのもと 結衣）
]]

local Phonetic = {}

--[[
	Soundexアルゴリズムに基づいて単語を変換する
	@param	a	元単語
	@return
]]
function Phonetic.soundex( a )
	local s = string.upper( a )
	local result = string.sub( s, 1, 1 )
	local last_code = ""
	local convert_table = {
		B = "1",
		F = "1",
		P = "1",
		V = "1",

		C = "2",
		G = "2",
		J = "2",
		K = "2",
		Q = "2",
		S = "2",
		X = "2",
		Z = "2",

		D = "3",
		T = "3",

		L = "4",

		M = "5",
		N = "5",

		R = "6",
	}

	for c in string.gmatch( utf8.charpattern ) do
		local code = convert_table[c]
		if code ~= nil and code ~= last_code then
			last_code = code
			result = result .. code
			if 4 < #result then
				break
			end
		else
			last_code = ""
		end
	end

	return string.sub( result .. "0000", 1, 4 )
end


--[[
	Caverphoneアルゴリズムに基づいて単語を変換する
	@param	a	元単語
	@return
]]
function Phonetic.caverphone( a )
	if #a == 0 then
		return "1111111111"
	end

	local s = string.lower( a )
	local convert_table = {
		-- 英語小文字以外は削除
		{ from = "[^a-z]", to = "" },

		-- 最後のeを削除
		{ from = "e$", to = "" },

		-- 先頭から始まるもの
		{ from = "^cough", to = "cou2f" },
		{ from = "^rough", to = "rou2f" },
		{ from = "^tough", to = "tou2f" },
		{ from = "^enough", to = "enou2f" },
		{ from = "^trough", to = "trou2f" },
		{ from = "^gn", to = "2n" },
		{ from = "^mb", to = "m2" },

		-- 置換
		{ from = "cq", to = "2q" },
		{ from = "ci", to = "si" },
		{ from = "ce", to = "se" },
		{ from = "cy", to = "sy" },
		{ from = "tch", to = "2ch" },
		{ from = "c", to = "k" },
		{ from = "q", to = "k" },
		{ from = "x", to = "k" },
		{ from = "v", to = "f" },
		{ from = "dg", to = "2g" },
		{ from = "tio", to = "sio" },
		{ from = "tia", to = "sia" },
		{ from = "d", to = "t" },
		{ from = "ph", to = "fh" },
		{ from = "b", to = "p" },
		{ from = "sh", to = "s2" },
		{ from = "z", to = "s" },
		{ from = "^[aeiou]", to = "A" },
		{ from = "[aeiou]", to = "3" },
		{ from = "j", to = "y" },
		{ from = "^y3", to = "Y3" },
		{ from = "^y", to = "A" },
		{ from = "y", to = "3" },
		{ from = "3gh3", to = "3kh3" },
		{ from = "gh", to = "22" },
		{ from = "g", to = "k" },
		{ from = "s+", to = "S" },
		{ from = "t+", to = "T" },
		{ from = "p+", to = "P" },
		{ from = "k+", to = "K" },
		{ from = "f+", to = "F" },
		{ from = "m+", to = "M" },
		{ from = "n+", to = "N" },
		{ from = "w3", to = "W3" },
		{ from = "wh3", to = "Wh3" },
		{ from = "w$", to = "3" },
		{ from = "w", to = "2" },
		{ from = "^h", to = "A" },
		{ from = "h", to = "2" },
		{ from = "r3", to = "R3" },
		{ from = "r$", to = "3" },
		{ from = "r", to = "2" },
		{ from = "l3", to = "L3" },
		{ from = "l$", to = "3" },
		{ from = "l", to = "2" },

		-- ハンドル削除
		{ from = "2", to = "" },
		{ from = "3$", to = "A" },
		{ from = "3", to = "" },
	}

	for _, t in pairs( convert_table ) do
		s = string.gsub( s, t.from, t.to )
	end

	return string.sub( result .. "1111111111", 1, 10 )
end

return Phonetic
